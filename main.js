var arrayNumber = [];

 document.getElementById("btnAddNumber").onclick = function() {

    var inputEl = document.getElementById("addNumber");
    if(inputEl.value == "") {
        return;
    }
    
    var number = inputEl.value * 1;
    arrayNumber.push(number);
    var inputEl = "";

    var content =`<p> Array hiện tại: ${arrayNumber} </p>`

    document.getElementById("showResult").innerHTML = content;
}

document.getElementById("btnSum").onclick = function() {
    var positiveNumber = 0;
    var positiveSum = 0;

    for ( var i = 0; i < arrayNumber.length; i++) {
        var number = arrayNumber[i];
        if(number > 0) {
            positiveNumber++;
            positiveSum += number;
        }
    }

    var contentHTML = `
        <p> Số lượng số dương: ${positiveNumber} </p>
        <p> Tổng số dương: ${positiveSum} </p>
        
    `
    document.getElementById("resultOfSum").innerHTML = contentHTML;
}

// Bài 3 + 4

document.getElementById("btnMinNumber").onclick = function() {

    var minNumber = arrayNumber[0];
    var minPositiveNunber = arrayNumber[0];
    
    for ( var i = 1; i < arrayNumber.length; i++) {
        var number = arrayNumber[i];

        if(minPositiveNunber > number) {
            if(number > 0)
            {
                minPositiveNunber = number;
            }
            if(minNumber > number)
                minNumber = number;
        }
    }

    if(minPositiveNunber < 0)
    {
        minPositiveNunber = "Không có số dương";
    }

    var result = `
        <p> Số nhỏ nhất: ${minNumber}</p>
        <p> Số dương nhỏ nhất: ${minPositiveNunber}</p>
        
    `
    document.getElementById("resultOfMin").innerHTML = result;
}

// Bài 5

document.getElementById("btnLastEvenNumber").onclick = function(){
    var lastEvenNumber = -1;

    for(var i = arrayNumber.length; i >= 0; i--){
        var number = arrayNumber[i]
        if(number % 2 == 0){
            lastEvenNumber = number;
            break;
        }
    }
    
    var result = `
        <p> Số chẵn cuối cùng: ${lastEvenNumber}</p>
    `

    document.getElementById("resultLastEvenNumber").innerHTML = result;
}

function swap(array, a, b){
    var temp = array[a];
    array[a] = array[b];
    array[b] = temp;
}

// Bài 7

document.getElementById("btnSortUp").onclick = function(){
    for(var i = 0; i < arrayNumber.length; i++){
        for(var j = 0; j < arrayNumber.length - i; j++){
            if(arrayNumber[j] > arrayNumber[j + 1]){
                swap(arrayNumber, j, j + 1);
            }
        }
    }

    var result = `
        <p> Mảng sau khi sắp xếp: ${arrayNumber} </p>
    `

    document.getElementById("resultSortUp").innerHTML = result;
}

// Bài 8
document.getElementById("btnFirstPrime").onclick = function(){
    var prime;

    for(var i = 0; i < arrayNumber.length; i++){
        if(arrayNumber[i] < 2 || (arrayNumber[i] % 2 == 0 && arrayNumber[i] != 2)){
            continue;
        }
        var count = 0;
        for(var j = 2; j < arrayNumber[i]; j++){
            if(arrayNumber[i] % j == 0){
                count++;
                break;
            }
        }
        if(count == 0){
            prime = arrayNumber[i];
            break;
        }
    }

    var result = `
        <p> Tìm số nguyên tố: ${prime} </p>
    `

    document.getElementById("resultFirstPrime").innerHTML = result;
}

// Bài 10
document.getElementById("btnCompare").onclick = function(){
    var countNegative = 0;
    var countPositive = 0;

    for(var i = 0; i < arrayNumber.length; i++){
        if(arrayNumber[i] == 0){
            break;
        }

        if(arrayNumber[i] > 0){
            countPositive++;
        }
        else{
            countNegative++;
        }
    }

    var temp = "";
    if(countPositive > countNegative){
        temp = "Số dương > Số âm";
    }
    else if(countNegative == countPositive){
        temp = "Số dương = Số âm";
    }
    else{
        temp = "Số âm > Số dương";
    }

    var result = `
        <p> ${temp} </p>
    `

    document.getElementById("resultCompare").innerHTML = result;
}